"Błędy są super" to prezentacja, która opisuje jak efektywnie uczyć się na swoich błędach.
Opowiada na przykładach jak zespołowo odkrywać błędy, odszukiwać przyczyny i uczyć się na nich.
Kierowana jest do kół naukowych, innych organizacji, czy pojedynczych osób, które chcą inwestować w swój rozwój.
Podczas prezentacji nauczysz się:
- Organizować *retrospektywy* w swoim zespole, które są przydatne w pracy jak i w samorozwoju
- Wykorzystywać narzędzia *5 times why* i *fishbone diagram* do odszukiwania przyczyn popełnionych błędów
- Wykorzystywać zasadę *SMART* do efektywnego definiowania zadań, które adresują problemy w zespole
