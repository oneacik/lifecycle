"Błędy są super" jest to prezentacja, która opisuje jak efektywnie uczyć się na swoich i cudzych błędach.
Opowiada na przykładach jak zespołowo odkrywać błędy, odszukiwać przyczyny i je naprawiać.
W obrazkowy sposób poprowadzi Cię przez temat prezentacji, by zakończyć się na auto-retrospektywie.

Kierowana jest do osób, które:
- Potrzebują podstawowych narzędzi do rozwoju zespołu
- Pomimo przeprowadzania retrospektyw w grupie, nie czują, że dostarcza ona wartość
- Poszukują metody do rozwijania się w codziennym życiu na własnych doświadczeniach

Czego nauczysz się na tej prezentacji:
- Otrzymasz dawkę przykładów, które pozwolą Ci lepiej zrozumieć na co warto zwracać uwagę na *retrospektywie*
- Organizować *retrospektywy* w swoim zespole, które pozwolą Ci iteracyjnie uczyć się na błędach i rozwijać się
- Wykorzystywać narzędzia *5 times why* i *fishbone diagram* do odszukiwania przyczyn popełnionych błędów
- Wykorzystywać zasadę *SMART* do efektywnego definiowania zadań, które adresują problemy w zespole

Do zobaczenia.
