void doSomethingA() { somethingA() }

void doSomethingB() { somethingB() }

@Around("execution(* *..doSomething*())")
public Object doBasicProfiling(ProceedingJoinPoint pjp) {
  var before = System.nanoTime()
  Object retVal = pjp.proceed()
  var after = System.nanoTime()

  var duration = after - before
  var functionName = pjp.getSignature().getName()

  sendAnalitic(new TimeMeasurement(functionName, duration))
  return retVal;
}
