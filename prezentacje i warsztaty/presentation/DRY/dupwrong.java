public void changeUserName(Integer id, String name) {
    Optional<User> user = userRepository.fetchUser(id)
    if (user.isEmpty()) // changed return type
        throw new NotFoundException(); // new exception
    user.get().setUserName(name)
    user.get().save();
}

public User getUser(Integer id) {
    var user = userRepository.fetchUser(id)
    if (user == null) // another line to change
        throw new NoSuchUserException(); // left old exception
    return user;
}
