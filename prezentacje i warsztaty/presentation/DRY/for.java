public List<Integer> squareElements(List<Integer> elements){
    List<Integer> squares = new ArrayList<>();
    for(Integer element: elements){
        squares.add(element * element)
    }
    return squares
}
