DRY - Functions, Aspects, Decorators

This presentation goes in depth of DRY (Don't Repeat Yourself) principle.
It focuses mostly on the Java language as a common ground for included examples.
It explains difference between basic copy duplication and pattern duplication.
It shows how to address duplication by using functions, aspects and decorators.

After this presentation you will understand what are pros and cons of each deduplication method.
You will gain inspiration which methods you can explore further to address yuor problems.
