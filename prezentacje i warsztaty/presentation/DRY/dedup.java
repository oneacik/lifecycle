public void changeUserName(Integer id, String name){
    var user = getUser(id);
    user.setUserName(name)
    user.save();
}

public User getUser(Integer id){
    var user = userRepository.fetchUser(id)
    if (user == null)
        throw new NoSuchUserException();
    return user;
}
