void doSomethingA(){
    var before = System.nanoTime()
    somethingA()
    var after = System.nanoTime()
    sendAnalitic(
        new TimeMeasurement("doSomethingA", after - before)
    )
}

void doSomethingB(){
    var before = System.nanoTime()
    somethingB()
    var after = System.nanoTime()
    sendAnalitic(
        new TimeMeasurement("doSomethingB", after - before)
    )
}
