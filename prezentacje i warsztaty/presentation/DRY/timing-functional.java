void doSomethingA() {
    measureTime("doSomethingA", () -> somethingA())
}

void doSomethingB() {
    measureTime("doSomethingB", () -> somethingB())
}

void measureTime(String functionName, Runnable in) {
    var before = System.nanoTime()
    in.run()
    var after = System.nanoTime()

    sendAnalitic(
        new TimeMeasurement(functionName, after - before)
    )
}

