public void changeUserName(Integer id, String name){
    var user = userRepository.fetchUser(id)
    if (user == null)
        throw new NoSuchUserException();
    user.setUserName(name)
    user.save();
}

public User getUser(Integer id){
    var user = userRepository.fetchUser(id) // dup
    if (user == null)                       // dup
        throw new NoSuchUserException();    // dup
    return user;
}
