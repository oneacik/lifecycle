@MeasureTime
doSomethingA(): void {
    somethingA()
}

@MeasureTime
doSomethingB(): void {
    somethingB()
}

function MeasureTime(target: any, functionName: string) {
  let before = window.performance.now()
  target[functionName]()
  let after = window.performance.now()
  let duration = after - before
  
  sendAnalitic(new TimeMeasurement(functionName, duration))
}
