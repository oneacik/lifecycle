## TIMERS PRESENTATION

1. Introduce me as Java programmer
1. How I learned to use peripherals in STM32
1. The Project: Three alternating diodes as stepstick metaphor with regulation. [make a quick movie]
1. One Condition - don't use processor after configuration.
1. I assumed that it was possible as I had some mild knowledge on timers and dma in STM32. [screen from  documentation index]

1. I started with generation of project via STM32CUBEMX -
1. It can generate nice cmake projects
1. However it bests works for generating clock configuration
1. <Problem> Code itself is usually quite mysterious and you still need to learn documentation to use it

1. To code I used CLion
1. It quite nicely handles cmake projects and have nice prompts

1. I created also .gdbinit file for gdb so it would automatically connect and upload the file
1. I could have added step to cmake that would run OpenOCD and execute the gdb.

1. First I had to understand how exactly timer in STM32 works
1. >Show image and explain the parts of it [use schema from documentation]
1. Introduce that sections are divided into two parts (explanations/registers) [draw]
1. Explain that in the rest of documentation are examples of using timers for particular scenarios.

1. My configuration. 1 master clock -> clocking three clocks -> clocks moved in phase [u need a nice diagram for it]
1. I then started to read through register documentation to know which registers I need to set for what.
1. <Important> I drew a mindmap to see clearly and more easily manipulate through registers [Got Picture <3]

1. <Problem> I didn't know what channels map to which outputs as it wasn't in documentation? [draw one]
1. <Important> It appeared that there are two types of documentation that are needed [make screenshot of second doc]

1. <Problem> Diodes finally started to blink but they were not correctly synchronised
1. I checked twice with documentation but couldn't understand the problem
1. Then I turned on timer stop for debugging and opened gdb.

GDB
1. First thing was checking the configuration registers to see if all bits got into
(STM32 is quite intelligent and won't allow to set invalid bits currently)
1. However all went well
1. Then I set debugger stop for timers and started to reproduce signals step by step
1. I had in my mind the values I should observe during the debugging
1. It appeared that timers were behaving correctly before getting reloaded
1. <Problem> I forgot to trigger UEV event which updated the register from their shadow values

ADC&DMA
1. Then I configured ADC and tried to get automatic DMA conversion working
1. However DMA was feeding timer prescaler with constant flow of ones.

GDB
1. I checked the status bits of ADC and DMA to be sure that everything is ok.
1. ADC had overrun bit turned on.
1. <Problem> I configured ADC first with DMA on before I configured

Summary:
1. I got the board working
1. Good tools to use:
    1. CLion - for students too. Good prompting and integration with stm32.
    1. OpenOCD - backend for gdb to connect to your board
    1. gdb - very nice console debugger with REPL
1. Possible problems you can encounter
    1. Remember about setting the clock
    1. Remember about checking configuration and status registers
    1. Some special order of operation may be crucial - like update bit or configuration
1. Tips
    1. Draw and organize your knowledge - Yo memory is too smol
    1. Lurk more documentation - you can learn more and solve more problems
    1. Make testing scenario. Try to understand what you should observe and then try to understand why u don't.
1. Boundary Scan? - Anybody knows if I could use it for Timer debugging?
1. ThankYouForPresentationAndHaveANiceDay - QUESTIONS!?!?
