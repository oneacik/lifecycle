// DIP violation: concrete class depending on concrete types - dependants may change
public record TypeParser() {
    // SRP violation: Different Database + More Types - accidential change may happen
    public void stringForDDL(Object data) {
        // OCP violation: adding more types needs change of data
        if (data instanceof String)
            return "NTEXT" 
        /***/

        throw IllegalArgumentException("No such db type");
    }
    /***/
}

public record TypeParser() {
    // change of contract: tests and code needs to be rewritten
    public void parseDataToQuery(Object data, dbType DatabaseType) {
        // rising complexity of the code
        if (data instanceof String && dbType.equals(ORACLE))
            return "NTEXT" ;
        if (data instanceof String && dbType.equals(MYSQL))
            return "LARGETEXT";
        if (data instanceof Integer && dbType.equals(ORACLE))
            return "NUMBER";
        if (data instanceof Integer && dbType.equals(MYSQL))
            return "INTEGER" 

        throw IllegalArgumentException("No such db type");
    }
    /***/
}
/***/
