public class BadUserService {
    // Service needs to know and test implementation of userRepository
    PostgresUserRepository userRepository;

    public BadUserService(PostgresUserRepository postgresUserRepository) throws SQLException {
        this.userRepository = postgresUserRepository; 
    }

    public JWTToken authenticate(String login, String password) {
        Optional<String> uid = null;
        try {
            uid = userRepository.findUserIdByLoginAndPassword(login, password);
        } catch (SQLException e) {
            throw new TemporaryErrorException();
        }

        return uid.map(x -> JWTFactory.fromId(x)).orElseThrow(x -> new NoSuchUserException());
    }
}


class UserBadServiceTest {
    public void whenSQLExceptionIsThrownThenTemporaryErrorExceptionIsThrown() {
        // How To Implement ???
    }

    @Test(expected = TemporaryErrorException.class)
    public void whenSQLExceptionIsThrownThenTemporaryErrorExceptionIsThrown() {
        BadUserService badUserService = new BadUserService(new TestingPostgresUserRepository());

        badUserService.authenticate("nouser", "password");
    }
}

class TestingPostgresUserRepository extends PostgresUserRepository {
    @Override
    public Optional<String> findUserIdByLoginAndPassword(String login, String password) throws SQLException {
        throw new SQLException();
    }
}
