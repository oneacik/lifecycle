class TypeParserFactory{
    public TypeParser createParserForMySQL(){
        new TypeParser(
            // ...
            new BasicType(DB.StringType, MySQL.DDLStringType),
            new BasicType(DB.IntegerType, MySQL.DDLIntegerType),
            // ...
        )
    }

    public TypeParser createParserForOracle(){
        new TypeParser(
            // ...
            new BasicType(DB.StringType, Oracle.DDLStringType),
            new BasicType(DB.IntegerType, Oracle.DDLIntegerType),
            // ...
        )
    }
}
