/*
#3 For SQLType<String> that matchesType(String.class)
   
CREATE TABLE people {
...
    name TEXT, #2 TEXT comes from string DDL
...
}

#1 "String" escaping - comes from parseDataToQuery
INSERT INTO people VALUES (... , "WOJTEK", ...)
*/

public interface SQLType<T> {
    public String parseDataToQuery(T data); // #1
    public String stringForDDL(); // #2
    public void matchesType(Class type); // #3
}
