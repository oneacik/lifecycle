interface TypeParser {
    public String parseDataToQuery(Object data);
}

public record TypeParserImpl(List<SQLType> sqlTypes) implements TypeParser {
    public String stringForDDL(Object data) {
        return sqlTypes.stream()
                .filter(type -> type.matchesType(data.getClass()))
                .findAny()
                .map(sqlType -> sqlType.stringForDDL()).orElseThrow(x ->
                        new IllegalArgumentException("No such db type")
                );
    }
}

@Configuration
class TypeParserFactory {
    @Bean
    TypeParser createForMysql() {
        return new TypeParserImpl(Arrays.asList(
                new BasicType(Mysql.StringType, new StringDataParser())
        ));
    }
}

@Configuration
class TypeParserFactory {
    @Bean
    @ConditionalOnProperty(name = "db.type", havingValue = "mysql")
    TypeParser createForMysql() {
        return new TypeParserImpl(Arrays.asList(
                new BasicType(Mysql.StringType, new StringDataParser()),
                new BasicType(Mysql.IntegerType, new IntegerDataParser())
        ));
    }

    @Bean
    @ConditionalOnProperty(name = "db.type", havingValue = "oracle")
    TypeParser createForMysql() {
        return new TypeParserImpl(Arrays.asList(
                new BasicType(Oracle.StringType, new StringDataParser()),
                new BasicType(Oracle.IntegerType, new IntegerDataParser())
        ));
    }
}
