class StringType implements SQLType<String> {
    parseDataToQuery(String data){
        return "\"" + DataUtils.escape(data) + "\""
    }

    stringForDDL(){
        return "LARGETEXT";
    }

    matchesType(Class type){
        return type.equals(String.class)
    }
}
///
public TypeParser createParserForMysql(){
    new TypeParser(/***/. new StringType(), /***/)
}
}
