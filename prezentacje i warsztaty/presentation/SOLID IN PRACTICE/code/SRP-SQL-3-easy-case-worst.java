class IntegerType implements SQLType<String> {
    parseDataToQuery(Integer data){
        return data.toString()
    }

    stringForDDL(){
        return "INT";
        // INT doesnt exist for Oracle - code broken
    }

    matchesType(Class type){
        return type.equals(String.class)
    }
}
///
public TypeParser createParserForMysql(){
    new TypeParser(/***/, new StringType(), new IntegerType(), /***/)
}

public TypeParser createParserForOracle(){
    new TypeParser(/***/, new StringTypeForOracle(), new IntegerType(), /***/)
}
