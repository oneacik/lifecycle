public class BadRenderer {
    Canvas canvas;
    BadRenderer(Canvas canvas) {
        this.canvas = canvas;
    }

    // ISP violation - do we really need file here?
    public void readAndRenderImage(File f) throws IOException {
        Image bufferedImage = ImageIO.read(f);
        canvas.getGraphics().drawImage(bufferedImage, 0, 0, null);
    }

    public void renderImageFromAws(URI s3Resource) throws IOException {
        File f = new File(s3Resource);
        readAndRenderImage(f);
    }

    public void renderImageFromBinary(byte[] image) throws IOException {
        // Unnecessary conversion
        File f = File.createTempFile("binary", ".png");
        try (OutputStream stream = new FileOutputStream(f)) {
            stream.write(image);
        }
        readAndRenderImage(f);
    }
}

public class NiceRenderer {
    Canvas canvas;
    NiceRenderer(Canvas canvas) {
        this.canvas = canvas;
    }

    public void readAndRenderImage(InputStream is) throws IOException {
        Image bufferedImage = ImageIO.read(is);
        canvas.getGraphics().drawImage(bufferedImage, 0, 0, null);
    }

    public void renderImageFromAws(URI s3Resource) throws IOException {
        readAndRenderImage(s3Resource.toURL().openStream());
    }

    public void renderImageFromBinary(byte[] image) throws IOException {
        readAndRenderImage(new ByteArrayInputStream(image));
    }
}
