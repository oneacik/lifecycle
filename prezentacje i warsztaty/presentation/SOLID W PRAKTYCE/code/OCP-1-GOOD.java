interface TypeParser {
    public String parseDataToQuery(Object data);
}

public record TypeParserImpl(List<SQLType> sqlTypes) implements TypeParser {
    public String stringForDDL(Object data) {
        return sqlTypes.stream()
                .filter(type -> type.matchesType(data.getClass()))
                .findAny()
                .map(sqlType -> sqlType.stringForDDL()).orElseThrow(x ->
                        new IllegalArgumentException("No such db type")
                );
    }
}

@Configuration
class TypeParserFactory {
    @Bean
    TypeParser createForMysql() {
        return new TypeParserImpl(Arrays.asList(
                new BasicType(DB.StringType,  MySQL.StringType),
        ));
    }
}

@Configuration
class TypeParserFactory {
    @Bean
    @ConditionalOnProperty(name = "db.type", havingValue = "mysql")
    TypeParser createForMysql() {
        return new TypeParserImpl(Arrays.asList(
                new BasicType(DB.StringType,  MySQL.DDLStringType),
                new BasicType(DB.IntegerType, MySQL.DDLIntegerType)
        ));
    }

    @Bean
    @ConditionalOnProperty(name = "db.type", havingValue = "oracle")
    TypeParser createForOracle() {
        return new TypeParserImpl(Arrays.asList(
                new BasicType(DB.StringType,  Oracle.DDLStringType),
                new BasicType(DB.IntegerType, Oracle.DDLIntegerType)
        ));
    }
}
