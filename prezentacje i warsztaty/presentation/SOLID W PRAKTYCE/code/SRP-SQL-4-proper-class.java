record BasicType<T>(Pair<Class<T>, DataParse> type, String ddlType) implements SQLType<T> {
    parseDataToQuery(String data){
        return pair.second().call(data);
    }

    stringForDDL(){
        return ddlType; 
    }


    matchesType(Class type){
        return pair.first().equals(type);
    }
}

class StringDataParse implements DataParse {
    public String call(){
        return "\"" + DataUtils.escape(data) + "\""
    }
}

