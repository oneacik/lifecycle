class IntegerType implements SQLType<String> {
    parseDataToQuery(Integer data){
        return data.toString()
    }

    stringForDDL(){
        return "INT";
        // Typ INT nie istnieje dla ORACLE - kod zepsuty
    }

    matchesType(Class type){
        return type.equals(String.class)
    }
}
///
public TypeParser createParserForMysql(){
    new TypeParser(/***/, new StringType(), new IntegerType(), /***/)
}

public TypeParser createParserForOracle(){
    new TypeParser(/***/, new StringTypeForOracle(), new IntegerType(), /***/)
}
