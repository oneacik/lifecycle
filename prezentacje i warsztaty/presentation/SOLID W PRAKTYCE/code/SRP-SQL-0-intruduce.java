/*
#3 Przyklad SQLType<String>, ktory matchesType(String.class) == true
   
CREATE TABLE people {
...
    name TEXT, #2 typ 'TEXT' z stringDDL
...
}

#1 dekorowanie cudzyslowami ("String") z parseDataToQuery
INSERT INTO people VALUES (... , "WOJTEK", ...)
*/

public interface SQLType<T> {
    public String parseDataToQuery(T data); // #1
    public String stringForDDL(); // #2
    public void matchesType(Class type); // #3
}
