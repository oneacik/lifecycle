// Naruszenie DIP: konkretna implementacja zamiast rozszerzenia interfejsu 
public record TypeParser() {
    // Naruszenie SRP: Rozne typy danych i rozne bazy danych
    public void stringForDDL(Object data) {
        // Naruszenie OCP: dodawanie nowych typow modyfikuje klase
        if (data instanceof String)
            return "NTEXT" 
        /***/

        throw IllegalArgumentException("No such db type");
    }
    /***/
}

public record TypeParser() {
    // Zmiana kontraktu: testy i kod zalezny musi zostac zmieniony
    public void parseDataToQuery(Object data, dbType DatabaseType) {
        // Rosnaca zlozonosc kodu
        if (data instanceof String && dbType.equals(ORACLE))
            return "NTEXT" ;
        if (data instanceof String && dbType.equals(MYSQL))
            return "LARGETEXT";
        if (data instanceof Integer && dbType.equals(ORACLE))
            return "NUMBER";
        if (data instanceof Integer && dbType.equals(MYSQL))
            return "INTEGER" 

        throw IllegalArgumentException("No such db type");
    }
    /***/
}
/***/
