interface UserRepository {
    public Optional<String> findUserIdByLoginAndPassword(String login, String password) throws SQLException;
}

public class GoodUserService {
    // Service opiera sie teraz na abstrakcji 
    UserRepository userRepository;

    public GoodUserService(UserRepository userRepository) throws SQLException {
        this.userRepository = userRepository;
    }

    public JWTToken authenticate(String login, String password) {
        Optional<String> uid;
        try {
            uid = userRepository.findUserIdByLoginAndPassword(login, password);
        } catch (SQLException e) {
            throw new TemporaryErrorException();
        }

        return uid.map(x -> JWTFactory.fromId(x)).orElseThrow(x -> new NoSuchUserException());
    }

    static public void createGoodUserService() throws SQLException {
        return new GoodUserService(new PostgresUserRepository());
    }
}

class UserServiceTest {
    @Test(expected = TemporaryErrorException.class)
    public void whenSQLExceptionIsThrownThenTemporaryErrorExceptionIsThrown() {
        BadUserService badUserService = new GoodUserService((user, pass) -> {
            throw new SQLException();
        });

        badUserService.authenticate("nouser", "password");
    }
}

public class Main {
    static void main(String[] args) {
        ConsoleController console = ConsoleController.create(
                    //...
                    GoodUserService.create()
                    //...
                )
        console.handle(args)
    }
}

public class LocalMain {
    static void main(String[] args) {
        ConsoleController console = ConsoleController.create(
                    //...
                    new GoodUserService(new H2Database()) 
                    //...
                )
        console.handle(args)
    }
}
