class IntegerType implements SQLType<Integer> {
    parseDataToQuery(Integer data){
        return data.toString();
    }

    stringForDDL(){
        return "INTEGER";
    }

    matchesType(Class type){
        return type.equals(Integer.class)
    }
}
