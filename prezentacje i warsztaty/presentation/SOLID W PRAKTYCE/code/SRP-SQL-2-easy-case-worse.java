class StringTypeForOracle implements SQLType<String> {
    parseDataToQuery(String data){
        return "\"" + DataUtils.escape(data) + "\""
        // duplikacja 
    }

    stringForDDL(){
        return "NTEXT";
    }
    ///...
}
///
public TypeParser createParserForMysql(){
    new TypeParser(/***/. new StringType(), /***/)
}

public TypeParser createParserForOracle(){
    new TypeParser(/***/, new StringTypeForOracle(), /***/)
}
