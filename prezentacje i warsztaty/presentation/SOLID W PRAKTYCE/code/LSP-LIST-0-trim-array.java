class TrimArrayTest {
    public <T> void trimList(List<T> list) {
        if (list.size() < 2) return;
        list.remove(0);
        list.remove(list.size() - 1);
    }

    @Test
    public void givenArrayList_testTrimArray() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(2);

        trimList(list);

        MatcherAssert.assertThat(list, Matchers.contains(1));
    }

    @Test
    public void givenAsList_testTrimArray() {
        List<Integer> list = Arrays.asList(0, 1, 2);

        // Throws:
        // java.lang.UnsupportedOperationException
	    //    at java.base/java.util.AbstractList.remove(AbstractList.java:167)
        trimList(list);

        MatcherAssert.assertThat(list, Matchers.contains(1));
    }
}

interface List<E>{
    /*
     * ...
     * @throws UnsupportedOperationException if the {@code remove} operation
     *         is not supported by this list
     * ...
     */
    E remove(int index);
}

    @Test
    public void givenLinkedArray_testTrimArray() {
        List<Integer> list = new LinkedList<>(Arrays.asList(0, 1, 2));

        trimList(list);

        MatcherAssert.assertThat(list, Matchers.contains(1));
    }
}
