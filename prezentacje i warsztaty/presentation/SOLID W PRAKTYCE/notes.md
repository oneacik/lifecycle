#SOLID

## S
--
### Cache, but seems that has two responsibilities 

### Violating but seems like has one reason to change:

interface SQLType<T> {
    public String parseDataToQuery(T data); // 
    public String stringForDDL(); // 
    public void matchesType(Class type);
}


CREATE TABLE DZIECI {
...
    imie TEXT,
...
}

INSERT INTO DZIECI VALUES (... , "WOJTEK", ...)

class StringType implements SQLType<String> {
    parseDataToQuery(String data){
        return "\"" + DataUtils.escape(data) + "\""
    }

    stringForDDL(){
        return "TEXT";
    }


    matchesType(Class type){
        return type.equals(String.class)
    }
}

var parserForMysql = new TypeParser(
...
new StringTypeForMySQL()
...
)

===

class StringTypeForOracle implements SQLType<String> {
    parseDataToQuery(String data){
        return "\"" + DataUtils.escape(data) + "\"" 
        // ^DUPLICATION OF FUNCTION WHICH COULD BE BE REALLY BIG!!!
    }

    stringForDDL(){
        return "NTEXT";
    }


    matchesType(Class type){
        return type.equals(String.class)
    }
}

var parserForMysql = new Parser(
...
new StringType()
...
)

var parserForOracle = new Parser(
...
new StringTypeForOracle()
...
)

===
OR WORSE

class StringType implements SQLType<String> {
    parseDataToQuery(String data){
        return "\"" + DataUtils.escape(data) + "\""
    }

    stringForDDL(){
        return "NTEXT";
    }


    matchesType(Class type){
        return type.equals(String.class)
    }
}


var parserForMysql = new Parser(
...
new StringType()
...
)

var parserForOracle = new Parser(
...
new StringType()
...
)



===

record BasicType<T>(Pair<Class, String> type, DataParse parseFunction) implements SQLType<T> {
    parseDataToQuery(String data){
        return parseFunction.call(data);
    }

    stringForDDL(){
        return pair.second();
    }


    matchesType(Class type){
        return pair.first().equals(type);
    }

}

//...

class Oracle {
    StringType = PairOf(String.class, "NTEXT")
}

//...

class StringDataParse implements DataParse {
    public String call(){
        return "\"" + DataUtils.escape(data) + "\""
    }
}


var parserForMysql = new Parser(
...
new BasicType(MySQL.StringType, new StringDataParser())
...
)

var parserForOracle = new Parser(
...
new BasicType(Oracle.StringType, new StringDataParser())
...
)



### Optional: Common DTO

SQL taking in List<Pair<String, Class>>


## O
Continue with Parser

## L
++
Immutable List In Java

## I
+++
Renderer:
Using File instead of InputStream

## D
+
Repository: 
Possibility to create other implementations looking on contract

# Why?

## SRP

## OCP

## LSP

## ISP

## DIP
