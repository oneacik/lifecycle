1. Introduction
    1. Title
    1. Plan Of presentation
1. Compiler/Interpreter
    1. Compiling/Interpreting
    1. Shelling - REPL
1. Project Manager
    1. Functions of Project Manager
    1. Present mvn/npm
    1. Project Structure
    1. Life Cycles
1. Dependencies 
1. Bundler
    1. Act Of bundling
    1. Differences in need of bundler
1. Special Part - Transpiler
    1. What is Transpiler
    1. Why it is needed in javascript
    1. Why it is not present in java
1. Interesting frameweworks for backend
    1. jest/junit
    1. knex.js/queryDSL
    1. express.js/Spring MVC
