var {Image} = require("image-js");
var fs = require("fs");
var path = require("path");

var imgInput = "images";
var imgOutput = "converted";


function process(path) {
    var canvas = new Image(1, 1, Uint8Array.from([255, 255, 255, 0, 0]), {
        components: 4,
        colorModel: "CMYK",
        bitDepth: 8,
        alpha: 1
    })
        .resize({width: 4960, height: 3720}); //4:3 slide
    //.resize({width: 5593, height: 3720}); //16:10 slide

    return Image.load(path)
        .then(function (image) {
            if (image.width < image.height) {
                return image.rotate(90);
            } else {
                return image;
            }
        })
        .then(function upscale(image) {
            return image.scale({width: 4960, height: 3496}); //A5 600 dpi = 4960x3496
        })
        .then(function cmykify(image) {
            //Math.max(image.max()) - Math.min(image.min());
            return image.cmyk();
        })
        .then(function normalize(image) {
            let [_1, _2, _3, kMax, _4] = image.max;
            let [_a, _b, _c, kMin, _d] = image.min;
            let ratio = 255 / (kMax - kMin);


            return (kMin > 0 ? image.subtract(kMin) : image)
                .multiply(ratio);
        })
        .then(function (image) {
            return canvas.insert(image, {
                x: Math.floor((canvas.width - image.width) / 2),
                y: Math.floor((canvas.height - image.height) / 2)
            });
        })
        .then(function (image) {
            var colors = [_1, _2, _3, k] = image.split();
            return k;
        });
}

fs.promises.readdir("images/")
    .then(files => Promise.all(files.map(file => {
        return process(path.join(imgInput, file))
            .then(img => img.save(path.join(imgOutput, file)))
            .then(_ => console.log(`${file} got converted`))
            .catch(err => console.error(`${file} failed`, err))
    })));

