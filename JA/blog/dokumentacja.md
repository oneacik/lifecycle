# Fuck Stack Overflow, Read The Fucking Manual

### Before we start

There are different types of documentation.
In this article I am talking about reference documentation teaching you technology step by step:

https://docs.docker.com/engine/reference/builder/

not javadoc, api style like:

https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/beans/annotation/AnnotationBeanUtils.html

## Introduction

Hello there.
I noticed that a lot of people tend to google stuff instead of learning technology properly.
And it seems like a standard...
Now 3 years of experience means three years of googling solutions from stack overflow and code is created by fuckup driven development.
It really pisses me off.
In this article I would like to show why googling stuff is bad and why you shoul LEARN!
Therefore I present you:

## 3 reasons why you should read the documentation

### Learn how to do stuff properly

Do you know what happens when you run tests in intellij idea?
Some strange magic happens, but it works, so nobody cares.
Ok, so now we have an integration test that starts MySQL database in docker in maven.
You can't have in memory database, because you have DDL queries in particular SQL and you want to test it too.
So now you have to run this test using `mvn integration-test` and it seems that debugging it from intellijidea is not working.

So now you are stucked and you have two options:

First Option: You start this database manually and run those tests from intellijidea.
Soon you realise that the database gets cluttered, so somehow you need to clear this database.
Therefore you manually recreate this database (you use docker so good for you) every time you need to test. This is the way of "I don't know how to solve the problem so let's avoid it".

Second Option: You search "how to debug tests in maven", you learn that there is `maven-failsafe-plugin` that runs this test for you and you add `debugForkedProcess` to its configuration, to make it debuggable. However then you would need to attach your debugger manually everytime you run this step. Still bad.

However, if you read the documentation, then you would know that `maven-failsafe-plugin` forks the testing process for greater isolation. That's why debugging maven step in intellijidea is not picking any breakpoints in the tests. If you would like to configure it to make it debuggable then, the easiest way is just to turn off the forking capability by setting `forkCount` to zero (however, it has its drawbacks, but after reading docco, you would know of them). Then intellijidea will be able to plug into tests without further problems.

As you see because of the documentation, you could understand how stuff works and therefore configure it to your needs. However, you could say: I would find it after some time nevertheless! Maybe you could, or maybe you would give up - I will counterargument this approach in the next section.

### Learn faster

Let's take some big framework for example (Hibernate for Java, Angular for frontend).
Big, complicated frameworks.
Let's simulate how much work you would need to do to learn it.

You go with google driven development, then everytime you don't know what to do, then you need to:
- google the question
- filter all questions that do not apply
- realise that found answer is not sufficient
- try to search more
- ask the question on stackoverflow
- get wrong answers
- finally get some snippset that started to work by accident

And when the problem is harder and more specific, the more research needs to be done to find it, great! And this one time, maybe, nobody will be able to answer it, then what? There are infinite possibilities of combining the code, finding your case may not be easy.

Of course this approach is better than reading documentation because it will take you only one day of doing 'almost nothing'. Investing three days for reading docco would be waste of time - you are paid for doing stuff, not knowing it, duh! Googling 30 days for 30 bugs is much better than 3 days of learning and then fixing stuff in 7 days. It is 3 times more money that you can spend for your burnout therapist. However reading about stuff is boring, isn't it?

### Learn to do 'boring' stuff

People tend to go around the boring stuff, they don't like to read books or spend time on planning - they love DOING (yes, we love wasting our precious time). In my opinion it comes from impulsive gathering of information from portals like stackoverflow or facebook that makes us this way. However, is reading books and documentation not fascinating? It would be, however we don't exercise our delayed gratification.

People usually meditate to clear their mind and be able to be focused for longer.
Try reading documentation for instance and you will notice the similar outcomes.

And you may think that it will be always boring and it will be always a pain.
However gratification will be there!
- In bugs you don't need to tackle
- With might that you will feel when using those libraries
- In time saved because you know what, how and why.
You will learn how to work smart not hard. And if you don't like gratification delayed, then think during reading about all problems you were having in the past that the new knowledge suddenly clarifies. I tend to get my motivation from this surge of programming power while reading - feel the power!

## Summary

Try binge reading doccos!
This approach may be hard at the beginning, but it will get better by time.
You will work faster, better and you will be more happy by not getting in the traps of inexperience and lack of knowledge.

For me the best thing is the fact that going this way allowed me to write the code the way I need and want, not the way that the google results say. It makes framework and libraries fit into your mindset, not your thinking fit into them!
