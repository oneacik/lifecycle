Cel:
Wprowadzenie do tematyki Springa i przedstawienie jego możliwości.
Konfiguracja środowiska pracy: Maven i IntelliJ IDEA. Wyjaśnienie mechanizmów IoC i DI.

1. Z czego się składa dzisiejsza prezentacja
1. Czym jest Spring
  1. Spring jako zbiór gotowych rozwiązań dla enterprise
    1. DI/REST/DATA/INTEGRATION
  1. Spring jako framework nie biblioteka
    1. Biblioteka -> My ją
    2. Framework -> On nas
    3. Przykład kodu
  1. Spring jako Dependency Injection Container
    1. Czym jest Dependency Injection,
    2. które pozwala nam na proste Dependency Inversion
    3. które pozwala nam na elastyczną aplikacje (przykład z Data Source dla MySQL i H2)
    4. Jest to również baza pod wszystkie inne funkcjonalności springa
  1. Spring jako prostszy ecosystem wobec Java EE
    1. Spring Core jako nakładka na Java EE (Controller budowany na stacku Servletów)
    2. Wspieranie podobnych annotacji jak Java EE + własne (Tabelka porównawcza)
    3. Spring nie wymaga wyspecializowanego kontenera do korzystania z części Java EE
    4. Spring jest prostszy od Java EE > Trzeba przeczytać całą dokumentacje springa by rozumieć jak dokładnie działa <magia>
1. Spring Boot
  1. Różnica między Spring Boot a Spring
    1. Spring Boot to autokonfiguracyjna nakładka na Spring (Spring + Autokonfiguracja + Serwer = Spring Boot)
    1. Porównanie ilości kodu, która jest potrzebna by postawić prostą aplikacje w Spring i Spring Boot
  2. Plusy i minusy spring boot
    1. (Mniej kodu, by postawić serwis) > (Więcej magii, która na szczęście jakoś działa)(Kiedy coś się zepsuje/trzeba dokonfigurować, to wracamy do springa)
    2. Z mojego doświadczenia: Stawianie springa jest dość męczące, gdzie Spring boot to czysta przyjemność

=== PRZERWA ===

1. Zadanie I - napisać prosty serwis zapamiętujący tekst i wypisujący go <to jeszcze doszlifuje jak sam je wykonam>
  1. wygenerować mvn archetype dla projektu mavenowego
  1. dodać dependencje dla spring-boot, flyway i hibernate
  1. dodać konfiguracje h2:mem do application.yml
  1. Stworzyć plik flyway /db/v01__create.sql, który tworzy tabelę create text { id Long; content TEXT;} <Checkpoint - tutaj można sprawdzić, czy appka się odpala>
  1. Stworzyć TextEntity, które składa się z pola id i pola content, dodać gettery settery i oznaczyć ją Entity, a id oznaczyć @Id
  1. Stworzyć klasę repozytorium, która rozszerza SimpleJdbcRepository<TextEntity>
  1. Dodać TextService, który ma metodę void addText(String) i List<String> getText(); i używa SimpleJdbcRepository list/add.
  1. Stworzyć klasę TextDto(String text) i TextViewModel(List<String> texts)
  1. Dodać @RestController TextController, który ma metody void postText(@RequestBody String body) i TextViewModel getTexts();
  1. Odpalić aplikacje używając wygenerowanego jar file
  1. Użyć postmana, by przetestować daną aplikacje.
1. Zadanie II - dodać usuwanie pojedynczych rekordów i dostawanie się do pojedynczych rekordów (możliwe, że do domu)
  1. Wykorzystać RequestMapping, DeleteMapping i GetMapping
  1. Wykorzystać placeholdery w adresie typu /{id}/ i @PathVariable Integer id w argumentach metod

1. Tipy na koniec
  1. Włączanie loggowania w Spring boot.
  1. Materiały do nauki własnej (Baeldung dla zielonych. Spring, spring boot reference dla odważnych)
