
Większość z nas jest tak zwanymi T-people.  
Mamy płytką wszechstronną wiedzę i jedną daleką specjalizację.  
Jeżeli jednak połączymy ze sobą ludzi paru specjalizacji, to otrzymujemy kwadrat,  
czyli grupę ludzi, którzy wzajemnie się uzupełniają swoimi kwalifikacjami.  
Poza tym budujemy relacje międzyludzkie, które są niezwykle cenne w czasach polaryzacji i podziału.
Niestety sieciowanie jest niezwykle trudne w tych czasach.


Po pierwsze, są to rzeczy nieinteresujące dla zwykłych pracowników.  
Jeżeli pracujemy już 8 godzin dziennie, to nie chcemy inwestować więcej czasu w naszą pracę.  
Ale chcemy spędzić miło czas i porozmawiać o ciekawych rzeczach po pracy.

Po drugie, łatwo jest zebrać podobnych ludzi do siebie zamiast różnorodnych.
Narzucając jeden temat, otrzymujemy wąską grupę ludzi,
narzucając networking zbieramy handlowców i osoby, które potrzebują tego do życia zawodowego.
Nie jest to dobre miejsce, jeżeli chcemy poznać osoby, które nas uzupełniają.

Po trzecie, ludzie często nawet nie wiedzą czego potrzebują.  
Tematy jak "Services Design", "Lean Startup", "Architektura Systemów" są nieznane zwykłym śmiertelnikom.

A jako teraz kończy się pandemia, to jest to idealna szansa na eksperymenty i zmiany.


Mój pomysł jest prosty. Chcę stworzyć spotkanie Show&Tell,  
który ma być właśnie takim zgrupowaniem różnorodnych osób.  
Chcę by na spotkaniu odbywały się 10-15 minutowe prezentacje,  
które są kierowane do szerokiej publiczności, i 10 minutowe przerwy między nimi.
Celem prezentacji ma być zainspirowanie osób do zainteresowania się danym tematem,
jak również przełamanie pierwszych lodów w rozmowie z prezentującym na przerwie.
Krótkość wystąpień ma pozwolić dużej ilości osób przedstawić się i ogranicza szanse
na to że dane spotkanie nie zainteresuje ludzi, którzy na nie przyjdą.
By utrzymać jakość tego spotkania, chcę również zapraszać ciekawe organizacje,
o różnych profilach, by utrzymać różnorodność i napływ nowych osób.


By osiągnąć ten cel potrzebuję jednak wsparcia.
Chciałbym zoorganizować focus group z 3 osobami, by lepiej wyprofilować to spotkanie.
Chcę zweryfikować, czy moje założenia są słuszne oraz wskazać kierunek rozwoju wydarzenia.
Chciałbym później zaprosić członków coworku, jeżeli formuła wydarzenia będzie im odpowiadać,
ponieważ sądzę, że są to interesujący ludzie i mają wiele do zaoferowania.

Na razie widzę 3 ścieżki dla wydarzenia:
- osoby opowiadają o swoich hobby - Jest to miękki i ciekawy temat. Ale w sumie nie przekazuje nic o prowadzącym jeżeli chodzi o umiejętności.
np: "Urban Exploring", "Historia alkoholów", "Czemu warto czytać Asimova"
- osoby opowiadają o tym czym się zajmują - Jest to bardzo szerokie pole do popisu i prezentacje mogą być naprawdę różnej jakości. Jest to dobry początek do dalszych rozmów.
np: "Jak to jest być growth hackerem", "Jak to być Product Managerem zdalnie z Kalifornią", "Problemy z bycia technical leaderem"
- osoby opowiadają o wykorzystaniu danej techniki - Tutaj inspirujemy ludzi technikami, które są na przydatne na codzień. Według mnie przynosi to największą wartość, ale mniej opowiada o nas.
np: "Show&Tell czyli jak tworzyć dobre prezentacje", "Clean Architecture, a życie na codzień", "Jak dobrze promować wydarzenia poza facebookiem".
