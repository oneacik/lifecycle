# Show&Tell: Opowieści przy kawie

## Opis wydarzenia

Mamy przyjemność zaprosić was na wydarzenie,
na którym będziecie mogli posłuchać, czy porozmawiać z osobami,
które opowiedzą o swoich kreatywnych projektach i interesujących hobby.

To doskonała okazja, by poszerzyć swoje horyzonty,
a przede wszystkim spędzić dobrze czas offline w dobie pandemi.

Każdy może przynieść swoje akcesoria, stworzone (lub wciąż tworzone) rzeczy, zdjęcia.
Usiąść przy stoliku i pochwalić się swoimi zajęciami.

Oprócz tego, można się zgłosić, by poprowadzić jedną z trzech 15 minutowych prelekcji, na początku spotkania.

Na razie zgłosili się:
- Piotr Suwała - Urban Exploring, co się kryje w zakamarkach miast.
- TBD - Tu możesz być Ty!
- TBD - Tu możesz być Ty!

Jeżeli nie wiesz, czy Twoje hobby/projekty nadają się na wydarzenie, to napisz do nas.

Do zobaczenia niebawem.
