# Projekt

Nazwa: MatchMaking Projektowy

## Canvas

### Grupy odbiorców

* Producenci (Ci, którzy szukają osób do projektów)
  * Stowarzyszenia/Fundacje
  * Organizacje polegające na dużej ilości wolontariuszy (np Stadion Narodowy)
  * Nowi przedsiębiorcy chcący wypróbować swój pomysł
* Konsumenci (Ci którzy chcą brać udział w projektach)
  * Wolontariusze
  * Osoby chcące rozwijać swoje kompetencje
* Ambiwalentni (Ci, którzy mogą przyjąć obie formuły)
  * Hobbiści
  * Aktywiści (sąsiedzi, aktywiści miejscy)

### Relacje z klientami

* Spotkania lokalne z użytkownikami aplikacji
* Trackery (np Jira) dla sugestii i bugów

### Kanały dystrybucji

* Media reklamowe partnerów
* Reklama na zasadzie wiralu aplikacji

### Propozycja wartości

* **Dopasowanie się formułą aplikacji do nowo kształtującego się człowieka cyfrowego**
* Narzędzia do prowadzenia projektu, które uczą i ułatwiają pracę (cyfrowa karta przebiegu projektu)
* **Karta przebiegu projektu, która udostępniona publicznie pozwoli uczyć się na błędach**
* Ułatwienie oddolnego prowadzenia projektów
* Łączenie ze sobą ludzi z umiejętnościami i narzędziami
* Oferta dla osób, które chcą prowadzić projekty (w przeciwieństwie do aplikacji, które kierują ofertę tylko dla wolontariuszy)
* Globalna aplikacja dla wolontariatu/projektów w całej polsce
* Tworzenie portfolio swoich projektów do wglądu przez obie strony
* Możliwość kierowania swoich działań lokalnie
* Baza kursów, która pozwoli się nauczyć prowadzenia projektów
* Baza wiedzy odnośnie pozyskiwania odpowiednich zgód dopasowana do projektu
* Stworzenie platformy do pozyskiwania dotacji

### Działania

* Przeprowadzić wywiady z grupami odbiorców
* Ustalić realne koszta prowadzenia projektów
* (Dla producentów)
  * Ustalić, czy potrzebują lepszej ścieżki pozyskiwania wolontariuszy/podwykonawców
  * Ustalić, czy potrzebują rozwijać swoje umiejętności organizacyjne
* (Dla konsumentów)
  * Ustalić co potrzebują, by zaangażować się w projekt
  * Ustalić czy formuła aplikacji im odpowiada

### Kluczowe zasoby

* Zespół programistyczny (umiejętności mogą się nakładać)
  * Backend Developer
  * Frontend Developer
  * Product Manager
  * Team Leader
  * DevOps [wdrożenie]
  * Quality Engineer
* Infrastruktura serwerowa/cloudowa (w zależności co wyjdzie taniej i lepiej)

### Partnerzy

* Organizacje wspierające NGO
  * sektor 3.0
  * inne
* Inkubatory przedsiębiorczości
* Organizacje wolontariackie

### Struktura Kosztów

* Utrzymywanie infrastruktury serwerowej/cloudowej
* Utrzymywanie zespołu developerskiego
* Opłaty za oprogramowanie wykorzystywane do developmentu

### Źródła przychodu

* Płatne partnerstwa (dla podmiotów szukających wolontariuszy non-stop)
* Opcje premium
  * Projekt prywatny
  * Promowanie projektu
* Dotacje od państwa (do zbadania)

### Otwarte Zasoby

* Biblioteki programistyczne na otwartych licencjach (MIT, CC, inne).

### Dobro Wspólne

* Ułatwienie realizacji projektów ideowych (społeczeństwo, kultura, środowisko)
* Wykształcenie umiejętności organizacyjnych w społeczeństwie
* Aktywizacja społeczeństwa

### Licencje

* Licencja propertiary do momentu wdrożenia, później opensource, by umożliwić współpracę w innych krajach
* API do pluginów na licencji opensource
